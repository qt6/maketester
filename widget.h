#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

#include <QList>

class QuestionsInfo;
class QListWidget;
class QLabel;
class QPushButton;


class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();


private:

    void loadFile();
    void saveFile();

    void updateList();


    QList<QuestionsInfo *>m_questions;

    QLabel *m_countQuestLbl;
    QListWidget *m_listW;
    QPushButton *m_saveBtn;
    QPushButton *m_addBtn;
    QPushButton *m_removeBtn;
    QPushButton *m_modifyBtn;

};
#endif // WIDGET_H
