#ifndef QUESTIONEDITFORM_H
#define QUESTIONEDITFORM_H

#include <QDialog>
#include <QList>
#include <QPair>

class QuestionsInfo;
class AnswersInfo;
class QFormLayout;
class QCheckBox;
class QTextEdit;

class QuestionEditForm : public QDialog
{
    Q_OBJECT
public:
    QuestionEditForm();
    QuestionEditForm(QuestionsInfo *info);

    QuestionsInfo *info()const;

private slots:

    void updateRight();
    void updateAnswer();


private:


    void addRow(AnswersInfo *ansInfo = nullptr);

    QuestionsInfo *m_info;
    AnswersInfo *m_ansInfo;

    QFormLayout *m_answLay;

    QList<QPair<QCheckBox*,QTextEdit*>>m_rows;

    QTextEdit *m_questW;


};

#endif // QUESTIONEDITFORM_H
