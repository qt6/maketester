#include "questioneditform.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif

#include <QPushButton>
#include <QCheckBox>
#include <QFormLayout>
#include <QLayout>
#include <QTextEdit>

#include "questionsinfo.h"
#include "answersinfo.h"

QuestionEditForm::QuestionEditForm()
{


    auto mainLay=new QVBoxLayout(this);
    auto btn1Lay=new QHBoxLayout(this);
    auto btn2Lay=new QHBoxLayout(this);
    m_answLay=new QFormLayout(this);


    m_questW = new QTextEdit(this);

    auto m_acceptBtn = new QPushButton("Accept", this);
    auto m_cancelBtn = new QPushButton("Cancel", this);

    auto m_addBtn = new QPushButton("Add", this);
    auto m_removeBtn = new QPushButton("Remove", this);


    m_info = new QuestionsInfo;


    m_removeBtn->setEnabled(false);

    btn1Lay->addWidget(m_addBtn);
    btn1Lay->addWidget(m_removeBtn);

    btn2Lay->addWidget(m_cancelBtn);
    btn2Lay->addWidget(m_acceptBtn);

    mainLay->addWidget(m_questW);
    mainLay->addLayout(m_answLay);
    mainLay->addLayout(btn1Lay);
    mainLay->addLayout(btn2Lay);


    connect(m_addBtn, &QPushButton::clicked,[this]{

        addRow();
        m_info->addAnswer();

    });

    connect(m_questW, &QTextEdit::textChanged,[this](){


        m_info->setQuestion(m_questW->toPlainText());

    });

    connect(m_acceptBtn, &QPushButton::clicked,[this]{

        accept();
    });

    connect(m_cancelBtn, &QPushButton::clicked,this,&QuestionEditForm::close);

}

QuestionEditForm::QuestionEditForm(QuestionsInfo *info)
    : QuestionEditForm()
{
    if ((m_info = info)) {
        m_questW->setText(m_info->question());

        for(auto answer:m_info->answers()){
            addRow(answer);
        }

    }
}

QuestionsInfo *QuestionEditForm::info() const
{
    return m_info;
}

void QuestionEditForm::updateRight()
{
    if(auto box = qobject_cast<QCheckBox*>(sender())){
        bool isFound=false;

        for(int index=0; index < m_rows.size(), !isFound; ++index){
            auto tmpBox = m_rows.at(index).first;

            if((isFound= (box==tmpBox))){

                auto answInfo = m_info->answers().at(index);

                answInfo->setIsRight(tmpBox->isChecked());

            }
        }
    }
}

void QuestionEditForm::updateAnswer()
{
    if(auto textField = qobject_cast<QTextEdit*>(sender())){

        bool isFound=false;

        for(int index=0; index < m_rows.size(), !isFound;++index){
            auto tmpField = m_rows.at(index).second;

            if((isFound= (tmpField==textField))){

                auto answInfo = m_info->answers().at(index);

                answInfo->setAnswer(tmpField->toPlainText());

            }
        }
    }
}

void QuestionEditForm::addRow(AnswersInfo *ansInfo)
{
    QCheckBox *tmpBox = new QCheckBox (this);
    QTextEdit *tmpText = new QTextEdit (this);

    if(ansInfo){
        tmpBox->setChecked(ansInfo->isRight());
        tmpText->setText(ansInfo->answer());
    }

    m_answLay->addRow( tmpBox, tmpText);
    m_rows.append(QPair<QCheckBox*, QTextEdit*>(tmpBox, tmpText));

    connect(tmpBox, &QCheckBox::stateChanged, this, &QuestionEditForm::updateRight);
    connect(tmpText, &QTextEdit::textChanged, this, &QuestionEditForm::updateAnswer);

}
