#include "widget.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif
#include <QApplication>

#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include <QLayout>
#include <QListWidget>
#include <QLabel>
#include <QPushButton>


#include "questionsinfo.h"
#include "questioneditform.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{

    setMinimumSize(640, 480);


    auto mainlay=new QVBoxLayout(this);

    QFile::copy(":/files/questionsFile",qApp->applicationDirPath()+"\\"+"questions.json");


    QFile::setPermissions("questions.json", QFile::WriteUser);



    m_listW=new QListWidget(this);
    m_countQuestLbl=new QLabel(this);
    m_saveBtn=new QPushButton("Save to file", this);
    m_addBtn=new QPushButton("Add question", this);
    m_removeBtn=new QPushButton("Remove question", this);
    m_modifyBtn = new QPushButton("Modify question", this);

    mainlay->addWidget(m_countQuestLbl);
    mainlay->addWidget(m_listW);
    mainlay->addWidget(m_saveBtn);
    mainlay->addWidget(m_addBtn);
    mainlay->addWidget(m_modifyBtn);
    mainlay->addWidget(m_removeBtn);

    m_removeBtn->setEnabled(false);

    connect(m_saveBtn,&QPushButton::clicked,[this]{

        saveFile();
    });

    connect(m_addBtn,&QPushButton::clicked,[this]{
        QuestionEditForm editForm;


        if(editForm.exec()){
            m_questions.append(editForm.info());
            updateList();
        }




    });

    connect(m_modifyBtn, &QPushButton::clicked, [this]{
        QuestionEditForm editForm(m_questions.at(m_listW->currentRow()));


        editForm.exec();


    });

    connect(m_saveBtn,&QPushButton::clicked,[this]{

        saveFile();
    });

    loadFile();
}

Widget::~Widget()
{
}

void Widget::loadFile()
{
    QFile loadFile(qApp->applicationDirPath()+"\\"+"questions.json");


    if(!loadFile.open(QFile::ReadOnly))
        return;

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));


    auto questArr = loadDoc.object()["Questions"].toArray();

    m_questions.clear();


    for(int index=0;index<questArr.size();++index){
        QuestionsInfo *quest=new QuestionsInfo;

        quest->read(questArr.at(index).toObject());

        m_questions.append(quest);

    }

    updateList();

}

void Widget::saveFile()
{
    QFile saveFile("questions.json");

    if(!saveFile.open(QFile::WriteOnly))
        return;

    QJsonObject docObj;
    QJsonArray questArr;

    for(auto quest:m_questions){
        QJsonObject questObj;

        quest->write(questObj);

        questArr.append(questObj);
    }

    docObj["Questions"] = questArr;

    saveFile.write(QJsonDocument(docObj).toJson());
}

void Widget::updateList()
{
    m_listW->clear();

    m_countQuestLbl->setText(QString("Count questions: %0").arg(m_questions.size()));

    for(auto question:m_questions)
        m_listW->addItem(question->question());

}

